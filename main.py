# import matplotlib as plt
import numpy as np
import cv2
import time
import os
from pylepton import Lepton



def RunCapture(i):
    with Lepton("/dev/spidev0.1") as l:
      a,_ = l.capture()
    cv2.normalize(a, a, 0, 65535, cv2.NORM_MINMAX) # extend contrast
    np.right_shift(a, 8, a) # fit data into 8 bits
    dt = GetCurrentUTCTimeFilename()	
    stringy = ['Image',"%s"%dt,'.jpg']
    st = "".join(stringy)
    cv2.imwrite("images/" + st, np.uint8(a)) # write it!
    
	
    
    
def GetCurrentUTCTimeFilename():
	print time.time()
	currentTime = time.gmtime()
	monthDay =  currentTime[2]
	hour =  currentTime[3]
	minute =  currentTime[4]
	second =  currentTime[5]
	fileName = "{monthDay}-{hour}-{minute}-{second}".format(**locals()) 
	print fileName
	return fileName
	

def InitDirectory():
	if not os.path.exists("images"):
	    os.makedirs("images")

if __name__ == '__main__':

    InitDirectory()
    i = 0
    while True:
        RunCapture(i)
        i += 1
        time.sleep(20)


